#!/usr/bin/env bash


#-----------HEADER-----------------------------------------------------------------|
# AUTOR             : Fernando Paschoeto <fernandopaschoeto@gmail.com>
# HOMEPAGE          : https://gitlab.com/fernandopaschoeto
# DATA-DE-CRIAÇÃO   : 25/06/2020
# PROGRAMA          : CiscoBackup
# VERSÃO	    : 1.4
# PEQUENA-DESCRIÇÃO : Programa para automatização de backups de IOS Cisco por TFTP
#                     
# DEPENDENCIAS:	    : tftpd-hpa, ssh, sshpass                  
# CHANGELOG : 
#
#-Versao-1.0 | Fernando Paschoeto Julho de 2020  
#	 	script funcional
#
#-Versao-1.1 | Fernando Paschoeto Julho de 2020  
#	 	blocos de hosts movidos para arquivo externo /hosts.conf
#	 	Criação de funções -h --help para ajuda e -V--version para versão
#	 	Adição de opção para backup individual de cada equipamento. 	
#
#-Versao-1.2 | Fernando Paschoeto Agosto de 2020  
#		Criação instruções de utilização do programa
#		no arquivo ciscobkp.info opção -h ou --help. 
#
#-Versao-1.3 | Fernando Paschoeto Agosto de 2020
#   		Criada variavel $log 
#   		Importando saida padrao e saida de erro para arquivo ciscobkp.log 
#   		Reestruturadas as mensagens informativas para melhorar aparencia do LOG
#
#-Versao-1.4 | Fernando Paschoeto Agosto de 2020
#   		Alterados todos os testes de de wget para ping. 
#
#-----------------------------------------------------------------------------------|

#---------------------------------- VARIÁVEIS ----------------------------------->
#
data=$(date +"%y%m%d-%H:%M")
log=/home/Usuario/CiscoBackup/CiscoBackup.log
#
#------------------------------------CHAMADAS-----------------------------------<
#
# Arquivo para adição de hosts a serem processados pelo programa CiscoBackup
source Hosts.conf
source CiscoBackup.info
# Direcionamento de saídas para LOG
exec 1>>${log}
exec 2>&1
#
echo "

        [$data] ======= INICIO DA ROTINA ...

"
#----------------------------------- PROGRAMA ---------------------------------->

case $1 in

	-b|--backup)
		#Chamada de funções (hosts) Para rotina de Backup. 
		SWITCH_01
		SWITCH_02
		SWITCH_03
	;;
	
	-h|--help)
		echo "$ajuda"
		exit 0
			
	;;

	-V|--version)
		echo -n $(basename "$0")
		echo
		grep "^# VERSÃO" "$0" | tail -1 | tr -d \#	
		exit 0
	;;

	#Chamada individual de hosts para backup único. 
	--switch01)

		SWITCH_01
		exit 0
	;;

	--switch02)

		SWITCH_02
		exit 0
	;;

	--switch03)
	
		SWITCH_03
		exit 0
	;;
	

	*)
		clear
		echo "opção inválida, consulte -h ou --help para mais informações. "	
		sleep 4s
				
esac

echo "

        [$data] ======= FIM DA ROTINA ...
"                      
