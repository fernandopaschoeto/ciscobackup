# Backups Automatizados em Switches Cisco. 



 O CiscoBackup.sh é um script desenvolvido em shell Bash com intuito de automatizar as rotinas de backup em switches Cisco. O script roda em qualquer servidor linux TFTP e pode ser executado automaticamente utilizando um gerenciador/ agenda de tarefas como o Crontab.  O Script foi testado em modelos Cisco Catalyst e Cisco Business SF-300. 

## Preparação do Switch 

##### Cisco Catalyst

Nos modelos Catalyst é possível realizar a criação de uma ACL permitindo a requisição de download do arquivo de configuração pelo servidor TFTP diretamente ao equipamento, aumentando assim a segurança do script. O servidor não precisa de nenhuma informação de acesso ao Switch para realizar o backup.

Os comandos a seguir, são utilizados em modo de configuração (configure terminal) no Switch para a criação da Access List. O endereço "192.168.1.200" deverá ser substituído pelo endereço IP do servidor TFTP. 

```` CISCO IOS
access-list 55 remark PERMIT hosts requesting TFTP access
access-list 55 permit host 192.168.1.200
tftp-server nvram:startup-config 55
````

##### Cisco Business SF-300

Em modelos Business SF-300, utilizei um bloco "EOF" para que o servidor acesse o equipamento e faça o download do arquivo de configuração. Neste caso a segurança pode ficar comprometida pois  é necessário adicionar informações de login e senha de acesso ao arquivo do script. Neste caso é aconselhável o servidor TFTP estar rodando apenas na rede de gerência sem receber um IP público ou acesso a internet. 

## Preparação do Servidor

Para que o script funcione corretamente é necessário ter o servidor linux com o TFTP instalado e devidamente configurado. Segue modelo de arquivo de configuração tftp-hpa no Ubuntu

```
nano /etc/default/tftpd-hpa

TFTP_USERNAME="tftp"
TFTP_DIRECTORY="/srv/tftp"
TFTP_ADDRESS="192.168.1.200:69"
TFTP_OPTIONS="--secure -c"
RUN_DAEMON="yes"
OPTIONS="-c -l -s /srv/tftp"
```

Segue a lista das dependências necessárias para o correto funcionamento do backup (Testado em servidor Ubuntu 18.04): 

- tftpd-hpa
- ssh
- sshpass
- Crontab

 Os pacotes *ssh* e *sshpass* são utilizados para acesso aos equipamentos do modelo Business SF-300 e o *Crontab* responsável por criar o agendamento das execuções da rotina de backup.

## O Script

Durante o desenvolvimento optei por dividir o script em dois arquivos *CiscoBackup.sh* e *Hosts.conf* dando a liberdade de alterar as configurações dos equipamentos sem a necessidade de edição no arquivo executável. Um terceiro arquivo *CiscoBackup.info* foi adicionado ao script contendo informações básicas sobre a execução da rotina e fica disponível através do comando: 

```shell
CiscoBackup.sh --help
```

Já o arquivo *CiscoBackup.log* é criado na primeira execução do script e passa a ser alimentado com as informações toda vez que for executado. 

Para execução automática utilizando o Crontab, o comando abaixo pode ser adicionado a tabela do *cron* e fará a rotina em todos os Hosts configurados. 

````shell
CiscoBackup.sh --backup
````

No exemplo do Script publicado foi utilizada a seguinte estrutura de Rede. 

![Diagrama](/img/diagrama.png)



 SWITCH_01<br> Endereço IP: 192.168.1.10<br> Modelo: Cisco SF-300

 SWITCH_02<br> Endereço IP: 192.168.1.20<br> Modelo: Catalyst 4500 / WS-C4948

 SWITCH_03<br> Endereço IP: 192.168.1.30<br> Modelo: Cisco Nexus 3064

 SERVIDOR_TFTP<br> Endereço IP: 192.168.1.200<br> Sistema Operacional: Ubuntu Server 18.04

## Conclusão

O script está funcional no momento, cumprindo sua tarefa no processo de backup porém, algumas questões ainda precisam ser melhoradas como o tratamento de erro caso a conexão com o host não esteja ativa e criptografar o arquivo *Hosts.conf* para ocultar informações de acesso aos equipamentos (Modelos SF-300). 

O projeto está disponível no [Link](https://gitlab.com/fernandopaschoeto/ciscobackup)


